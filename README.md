# Methods in Cell Analysis and Laboratory Automation

**Lab Course for Biotechnology Master Students - Image Analysis Fundamentals**

When you first log in to RRP [https://rrp-tst.ethz.ch](https://rrp-tst.ethz.ch), you will find four folders.

```bash
build   # Should not be modified.
openbis # In here you find the necessary data for the course.
project # In this folder, you find all the course material.
results # This is the folder where you should save your results.
```

Navigate to the `project` folder and start with a new notebook for your assignments (File -> New -> Notebook). To load the required data set `plate01.nd2` you can run the following lines in your notebook:

```python
from pathlib import Path
base = Path.home()
Path(base / 'openbis/raw_data/20240909104629344-42759/original/plate01.nd2')
```
